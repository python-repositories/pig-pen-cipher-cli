# Pigpen Cipher Encoder

This tool encodes text files into HTML documents using the Pigpen cipher, replacing characters with images representing the cipher's symbols.
## Example Output
![Example Symbols](./example_output.png)
[Here](./html_outputs/example_output.html) is a fully converted text as well.

[TOC]

## Quick Start

1. **Clone the repository:**
   ```sh
   git clone git@gitlab.com:python-repositories/pig-pen-cipher-cli.git
   ```
2. **Navigate to the project directory:**
   ```sh
   cd pig-pen-cipher-cli
   ```
3. **Run the script with the required arguments:**
   ```sh
   python cipher.py -t inputs/The_Bonehunters_Chapter_14.txt -f configs/starter_cipher.json -o output.html
   ```

<details><summary>## Installation and Setup</summary>
<p>

### Prerequisites

- Python 3.11+
- Pipenv

### Installing Dependencies

1. **Initialize Pipenv (if not already done):**
   ```sh
   pipenv --python 3.11
   ```
2. **Activate the Pipenv shell:**
   ```sh
   pipenv shell
   ```
3. **Install required packages (if any):**
   ```sh
   pipenv install <package-name>
   ```

</p>
</details>

### Script Arguments

- `-t`, `--text-file`: Path to the text file to be ciphered.
- `-f`, `--config`: Path to the cipher configuration JSON file.
- `-o`, `--output`: Name for the output HTML file (e.g., output.html).
- `--print-key`: Creates an HTML Key of your cipher when it creates the ciphered text.
### Running the Script

Run the script by providing the paths to your text file, configuration file, and the desired output HTML file name:

```sh
python cipher.py -t inputs/The_Bonehunters_Chapter_14.txt -f configs/starter_cipher.json -o output.html
```
Run the script with the key flag:
```sh
python cipher.py -t inputs/The_Bonehunters_Chapter_14.txt -f configs/starter_cipher.json -o output.html --print-key
```


<details><summary>## Configuration</summary>
<p>

### Cipher Configuration

Modify the `config.json` file in the `/configs` directory to change the cipher mappings. Each letter should map to an image file representing its corresponding symbol.

### Image Directory

Place all cipher symbol images in the `/images` directory. The script references these images when encoding text. There is a version that includes the numbers 0-9 as well out there.

</p>
</details>

<details><summary>## Troubleshooting</summary>
<p>

### Common Issues

- **Images not displaying in HTML:** Ensure the paths in `config.json` match the image files in the `/images` directory. Check that the script is run from the correct directory.

### Logging

The script uses Python's logging module. Check the logs for warnings or errors that can provide more information about any issues.

</p>
</details>

<details><summary>## Contributing</summary>
<p>

I welcome contributions and suggestions! Please open an issue or pull request with your improvements or ideas.

</p>
</details>

### Acknowledgements
*The scribblers were closing in on all sides, you see.*

*Steven Erikson and Malazan Book of the Fallen*

## Contact

For support or queries, reach out to @shivonq or submit issues on gitlab.