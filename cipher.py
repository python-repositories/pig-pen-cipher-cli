from datetime import datetime
import json
import logging
import argparse
from pathlib import Path
import os

# Configure logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

# Load the cipher configuration from a JSON file
def load_cipher_config(config_path: str) -> dict[str, any]:
    try:
        with open(config_path, 'r') as file:
            return json.load(file)
    except FileNotFoundError:
        logging.error(f"The configuration file {config_path} was not found.")
        raise
    except json.JSONDecodeError:
        logging.error(f"The configuration file {config_path} could not be decoded.")
        raise

# Encode the text using the specified cipher configuration
def encode_pigpen(text: str, cipher_config: dict[str,str], image_dir: str) -> str:
    encoded_html = ''
    for char in text:
        if char == ' ':
            encoded_html += '&nbsp;'
        elif char == '\n':
            encoded_html += '<br>'
        elif char.lower() in cipher_config:
            img_filename = cipher_config[char.lower()]
            img_path = f"../{image_dir}/{img_filename}"
            encoded_html += f'<img src="{img_path}" alt="{char}"/>'
        elif char in ".,!?:;-'\"":  # Add other punctuation as needed
            encoded_html += f'<span class="punctuation">{char}</span>'
        elif char.isdigit():  # Check if the character is a number
            encoded_html += f'<span class="numbers">{char}</span>'  # Wrap numbers in a span with a class
        else:
            encoded_html += char  # Keep special characters as is
    return encoded_html

# Generate the HTML content
def generate_html(encoded_text: str) -> str:
    return f"""
    <!DOCTYPE html>
    <html>
    <head>
        <title>Ciphered Text</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {{
                padding: 20px;
            }}
            .cipher-text {{
                white-space: pre-wrap;      /* Wraps the text */
                word-wrap: break-word;      /* Break the word */
                font-family: 'Courier New', monospace; /* Ensures consistent sizing for images */
                line-height: 1.6;          /* Adjust line height for vertical spacing */
            }}
            .cipher-text img {{
                vertical-align: bottom;  /* Align images to the bottom */
                padding-bottom: 34px; /* makes room for people to write a printed out versions correct message */
            }}
            .punctuation {{
                font-size: 2.2em;      /* Make punctuation larger */
                font-weight: bold;      /* Make punctuation bold */
                position: relative;
                bottom: -0.6em;         /* Adjust vertical position */
            }}
            .numbers {{
                font-size: 2.2em;       /* Adjust to match the size of cipher symbols */
                font-weight: bold;      /* Make numbers bold */
                position: relative;     /* Allow precise positioning adjustments if needed */
                bottom: -0.42em;        /* Adjust vertical position */

            }}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="cipher-text">{encoded_text}</div>
        </div>
    </body>
    </html>
    """

# Save the HTML content to a file
# TODO: Figure out what exactly the correct type for this is.
def save_html(content, output_path):
    try:
        with open(output_path, 'w') as file:
            file.write(content)
        logging.info(f"HTML file successfully saved to {output_path}")
    except Exception as e:
        logging.error(f"Failed to save HTML file to {output_path}: {e}")

# Generate a key for the cipher you provide
def generate_cipher_key_html(cipher_config:dict[str, dict[str, str]], image_dir: str) -> str:
    html_content = """
    <!DOCTYPE html>
    <html>
    <head>
        <title>Cipher Key</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>
            body {
                padding: 20px;
            }
            .cipher-text, .cipher-key {
                white-space: pre-wrap;      
                word-wrap: break-word;      
                font-family: 'Courier New', monospace;
                line-height: 1.6;          
            }
            .cipher-text img, .cipher-key img {
                vertical-align: middle;
                padding-right: 5px;
            }
            .cipher-letter {
                display: inline-block;
                padding-left: 5px;
                font-weight: bold;
                font-size: 1.2em;
                vertical-align: middle;
            }
            .cipher-key {
                -webkit-column-count: 2;
                -moz-column-count: 2;
                column-count: 2;
                -webkit-column-gap: 40px;
                -moz-column-gap: 40px;
                column-gap: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h2>Cipher Key</h2>
            <div class="cipher-key">
    """

    # Ensure cipher_config is a dictionary
    if not isinstance(cipher_config, dict):
        logging.error("Cipher configuration is not a valid dictionary.")
        return ""
    
    # TODO: Come back and list comprehension this.
    for section, mappings in cipher_config.items():
        for letter, img in mappings.items():
            img_path = f"{image_dir}/{img}"  # Adjusted path for web viewing
            html_content += f'<div><img src="{img_path}" alt="{letter}"> {letter.upper()}</div>\n'
    
    html_content += """
            </div>
        </div>
    </body>
    </html>
    """
    return html_content



# Main function to orchestrate the process
def main(args):
    images_dir = "../images"  # Assumes images directory is correctly specified
    cipher_map = load_cipher_config(args.config)
    print(cipher_map)
    print("Type:"+ str(type(cipher_map)))
    
    if args.print_key:
        # Path for generating cipher key HTML
        timestamp = datetime.now().strftime("%Y-%m-%d-%H%M%S")
        output_path = f"html_outputs/{timestamp}-cipher-key.html"
        html_content = generate_cipher_key_html(cipher_map, images_dir)
        save_html(html_content, output_path)
        return  # Exit after handling print key to avoid further processing

    # The rest of the script for encoding and saving encoded text
    output_path = Path("html_outputs") / args.output
    with open(args.text_file, 'r') as file:
        text_to_be_ciphered = file.read()
    encoded_text = encode_pigpen(text_to_be_ciphered, cipher_map, images_dir)
    html_content = generate_html(encoded_text)
    save_html(html_content, output_path)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Encode text using a pigpen cipher and generate an HTML output.')
    parser.add_argument('-t', '--text-file', required=True, help='Path to the text file to be ciphered')
    parser.add_argument('-f', '--config', required=True, help='Path to the cipher configuration JSON file')
    parser.add_argument('-o', '--output', required=True, help='Name for the output HTML file (e.g., output.html)')
    parser.add_argument('--print-key', action='store_true', help='Generate an HTML output of the cipher key')

    args = parser.parse_args()
    main(args)
